import sqlite3
import re
from flask import Flask, request
from utils import execute_sql

app = Flask(__name__)


@app.route("/phones/create/")
def create():

    contact_name = request.args.get('name')
    phone_value = request.args.get('phoneValue')
    if re.match(r'^\d{10}$', phone_value) is None:
        return 'Input correct phoneValue'
    sql = f'''
    INSERT INTO Phones (contactName, phoneValue)
    VALUES ('{contact_name}', '{phone_value}');
    '''
    execute_sql(sql)
    return 'record create successfully'


@app.route("/phones/read/")
def read():
    con = sqlite3.connect('phones.db')
    cur = con.cursor()
    sql = '''
    SELECT * FROM Phones;
    '''
    res = cur.execute(sql)
    phones = res.fetchall()
    con.close()
    return phones


@app.route("/phones/update/")
def update():
    contact_id = request.args.get('id')
    phone_value = request.args.get('phoneValue')
    if re.match(r'^\d{10}$', phone_value) is None:
        return 'Input correct phoneValue'
    sql = f'''
    UPDATE Phones
    SET phoneValue = '{phone_value}'
    WHERE phoneID = '{contact_id}';
    '''
    execute_sql(sql)
    return f'record (id - {contact_id}) update successfully'


@app.route("/phones/delete/")
def delete():
    contact_id = request.args.get('id')
    sql = f'''
       DELETE FROM Phones WHERE PhoneID = '{contact_id}';
        '''
    execute_sql(sql)
    return ''


if __name__ == '__main__':
    app.run()
