import sqlite3


def execute_sql(sql: str):
    con = sqlite3.connect('phones.db')
    cur = con.cursor()
    cur.execute(sql)
    con.commit()
    con.close()
